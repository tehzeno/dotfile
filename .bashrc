echo " --- loading .bashrc --- " 

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

export PATH=/opt/subversion/bin:$PATH:/Users/Shared/android-sdk-macosx/tools:/Users/zeno/ProjectsV5/AIQ-MAIL-MVCMobile/AIQ8/play20-installbase:/usr/local/mysql/bin
export SVN_EDITOR=vim
export CC=gcc

################################################################################
# TERM COLORS 
################################################################################
alias termcolor="export TERM=xterm-color"
alias termfullcolor="export TERM=xterm-256color" 

termfullcolor


alias lowercase="find . -type f -exec echo mv {} \`echo {} \| tr A-Z a-z\` \; | sh"

#Reload Bashrc
alias reload='source ~/.bashrc'

#Save history correctly when using multiple terminals
# Dont save duplicate lines or blank lines in to history
export HISTCONTROL=ignoreboth
export HISTSIZE=1000
#Append changes to history instead of overwrite full file
alias exit='history -a && exit'

#Bind history tab complete stuff
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*


# vagrant
export VAGRANT_DEFAULT_PROVIDER=parallels

################################################################################
# VCSH 
################################################################################
vcsh_main_repo="dotfiles"
alias dotpush="vcsh $vcsh_main_repo push origin master"
alias dotpull="vcsh $vcsh_main_repo pull -u origin master"
alias dotadd="vcsh $vcsh_main_repo add"
alias dotcommit="vcsh $vcsh_main_repo commit --message"
alias dotstate="vcsh $vcsh_main_repo status"
alias dotresetfile="vcsh $vcsh_main_repo checkout" # file here
function permalias () 
{ 
    alias "$*";
    echo alias \"$*\" >> ~/.bash_aliases
}
source ~/.bash_aliases

function tabname {
  printf "\e]1;$1\a"
}

function winname {
  printf "\e]2;$1\a"
}

function showart () {
    green='\033[0;32m' # '\e[1;32m' is too bright for white bg.
    endColor='\033[0m'
    red='\033[0;31m'
    purple='\033[0;35m'

    # Display welcome message
    echo -e "${purple}"
    cat ~/.bash_art
    echo " " 
    echo -e "${green} $USER @ ${red} `hostname` \n${endColor}  "
}

showart 
