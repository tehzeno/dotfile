function load_alias_functions
  set -g vcsh_main_repo "dotfiles"  
  alias dotpush="vcsh $vcsh_main_repo push origin master"
  alias dotpull="vcsh $vcsh_main_repo pull -u origin master"
  alias dotadd="vcsh $vcsh_main_repo add"
  alias dotcommit="vcsh $vcsh_main_repo commit --message"
  alias dotstate="vcsh $vcsh_main_repo status"
  alias dotresetfile="vcsh $vcsh_main_repo checkout" # file here
end
